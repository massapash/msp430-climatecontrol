#include <msp430g2553.h>
#include <stdint.h>
#include <stdlib.h>

#define CLOCK 16000000
#define TA_MAX 65535
#define TA_RATE (CLOCK / TA_MAX)
#define FAN_PWM_PERIOD 1900 // 9kHz
#define LED_PWM_PERIOD 16383 // 1kHz
#define TA0_OFFSET (TA_MAX - FAN_PWM_PERIOD)
#define TA1_OFFSET (TA_MAX - LED_PWM_PERIOD)
#define TA1_HZ (TA_RATE * (TA_MAX / LED_PWM_PERIOD))
#define FAN_BOOST_TIME (1 * TA1_HZ) // 1 second
#define TEMP_MEASURE_PERIOD (60 * TA1_HZ) // 60 seconds
#define MEASURE_CNT 10
#define UART_DIVIDER 104
#define UART_RX BIT1
#define UART_TX BIT2
// Internal temp. sensor calibration data
#define CAL_ADC_15T30 (*((unsigned *)(0x10DA + 0x08)))
#define CAL_ADC_15T85 (*((unsigned *)(0x10DA + 0x0A)))
#define HEATER BIT0 // now uses reversed key

#define WIND_PWM TA0CCR0
#define WIND_FAN BIT5
#define COOL_PWM TA0CCR1
#define COOL_FAN BIT6
#define WHITE_PWM TA1CCR0
#define WHITE_LED BIT0
#define BLUE_PWM TA1CCR1
#define BLUE_LED BIT2
#define RED_PWM TA1CCR2
#define RED_LED BIT4

const char *out_str = 0;
const char unknown_str[] = "unknown";
char number_str[] = "-65535";
uint8_t str_iter = 0;

char cmd_buf[] = "0000";
uint8_t cmd_iter = 0;

// Always set this when changing fan PWM
unsigned int fan_boost = FAN_BOOST_TIME;

// %
uint8_t blue_pwm_lvl = 100;
uint8_t red_pwm_lvl = 100;
uint8_t white_pwm_lvl = 50;
uint8_t wind_pwm_lvl = 1;
uint8_t cool_pwm_lvl = 1;

unsigned temp_measure_period = TEMP_MEASURE_PERIOD;
unsigned keep_temp = 250; // keep this temp
int celsius_temp = 0; // current temp
unsigned temp_measure_series[MEASURE_CNT] = {0};

static inline int adc_to_celsius(unsigned temp)
{
    return ((long)temp - CAL_ADC_15T30) * (85 - 30) * 10 / (CAL_ADC_15T85 - CAL_ADC_15T30) + 300;
}

static void setup_SMCLK(void)
{
    // Stop watchdog
    WDTCTL = WDTPW + WDTHOLD;
    // Calibrate basic clock system to 16MHz at 30C and 3V
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;
}

static void setup_UART(void)
{
    // Use GPIO ports 1.1 and 1.2 for RX/TX
    P1SEL |= UART_RX + UART_TX;
    P1SEL2 |= UART_RX + UART_TX;
    // Use SMCLK for UART
    UCA0CTL1 |= UCSSEL_2;
    // Set baud to 9600 (see doc.)
    UCA0BR0 = UART_DIVIDER;
    UCA0BR1 = 0;
    // Modulation and oversampling
    UCA0MCTL = UCBRS0 + UCBRF3 + UCOS16;
    // Initialize USCI state machine
    UCA0CTL1 &= ~UCSWRST;
    // Enable USCI_A0 RX interrupt
    UC0IE |= UCA0RXIE;
}

static inline uint16_t pwm_fill(uint16_t period, uint8_t percent)
{
    return TA_MAX - (period - percent * (period / 100));
}

static void setup_PWM(void)
{
    // Timer A0 outputs
    P1DIR |= (WIND_FAN + COOL_FAN);
    P1SEL |= (WIND_FAN + COOL_FAN);
    // Kickstart fans
    WIND_PWM = COOL_PWM = TA_MAX;
    // Timer A1 outputs
    P2DIR |= (WHITE_LED + BLUE_LED + RED_LED);
    P2SEL |= (WHITE_LED + BLUE_LED + RED_LED);
    // Set led brightness
    WHITE_PWM = pwm_fill(LED_PWM_PERIOD, white_pwm_lvl);
    BLUE_PWM = pwm_fill(LED_PWM_PERIOD, blue_pwm_lvl);
    RED_PWM = pwm_fill(LED_PWM_PERIOD, red_pwm_lvl);
    // Setup comparators mode to PWM reset
    TA0CCTL0 = TA0CCTL1 = TA1CCTL0 = TA1CCTL1 = TA1CCTL2 |= OUTMOD_5;
    // Set timers source to SMCLK, continuous mode, irq
    TA0CTL = TA1CTL |= TASSEL_2 + MC_2 + TAIE;
}

static inline void start_ADC_conv(void)
{
    // If ADC busy back off
    if (~(ADC10CTL1 & BUSY)) {
        // Renew series result buffer
        ADC10SA = temp_measure_series;
        // Start conversion
        ADC10CTL0 |= ENC + ADC10SC;
    }
}

static void setup_ADC(void)
{// Use maximum sampling time for internal sensor (64 ticks at 5MHz/8)
    // Stop conversion before setup
    ADC10CTL0 &= ~ENC;
    // Internal temp. sensor channel, 5MHz/8, series of measures
    ADC10CTL1 = INCH_10 + ADC10DIV_7 + CONSEQ_2;
    // Turn on, irq, reference generator, ref.buf., 64 ticks, multiple conversions
    ADC10CTL0 = ADC10ON + ADC10IE + REFON + SREF_1 + ADC10SHT_3 + MSC;
    // Series size
    ADC10DTC1 = MEASURE_CNT;
    // Actor
    P1DIR |= HEATER;
    P1OUT |= HEATER;

    start_ADC_conv();
}

static inline void set_cmd_handler(char dev_id, uint8_t level)
{
    if (level > 100) return;

    switch (dev_id) {
        case 'W':// Set white led level
            white_pwm_lvl = level;
            if (white_pwm_lvl) {
                P2SEL |= WHITE_LED;
                WHITE_PWM = pwm_fill(LED_PWM_PERIOD, white_pwm_lvl);
            } else {
                WHITE_PWM = pwm_fill(LED_PWM_PERIOD, 1);
                P2SEL &= ~WHITE_LED;
            }
            break;
        case 'B':// Set blue led level
            blue_pwm_lvl = level;
            if (blue_pwm_lvl) {
                P2SEL |= BLUE_LED;
                BLUE_PWM = pwm_fill(LED_PWM_PERIOD, blue_pwm_lvl);
            } else {
                BLUE_PWM = pwm_fill(LED_PWM_PERIOD, 1);
                P2SEL &= ~BLUE_LED;
            }
            break;
        case 'R':// Set red led level
            red_pwm_lvl = level;
            if (red_pwm_lvl) {
                P2SEL |= RED_LED;
                RED_PWM  = pwm_fill(LED_PWM_PERIOD, red_pwm_lvl);
            } else {
                RED_PWM  = pwm_fill(LED_PWM_PERIOD, 1);
                P2SEL &= ~RED_LED;
            }
            break;
        case 'S':// Set wind fan level
            wind_pwm_lvl = level;
            if (wind_pwm_lvl) {
                P1SEL |= WIND_FAN;
                WIND_PWM = TA_MAX;
                fan_boost = FAN_BOOST_TIME;
            } else {
                WIND_PWM = pwm_fill(FAN_PWM_PERIOD, 1);
                P1SEL &= ~WIND_FAN;
            }
            break;
        case 'C':// Set cooling fan level
            cool_pwm_lvl = level;
            if (cool_pwm_lvl) {
                P1SEL |= COOL_FAN;
                COOL_PWM = TA_MAX;
                fan_boost = FAN_BOOST_TIME;
            } else {
                COOL_PWM = pwm_fill(FAN_PWM_PERIOD, 1);
                P1SEL &= ~COOL_FAN;
            }
            break;
        case 'T':// Set temperature level
            keep_temp = level * 10;
            break;
        case 'V':// Set water valve open time
            break;
    }
}

#pragma vector=TIMER0_A1_VECTOR
__interrupt void TA0_ISR(void)
{
    if (TA0IV & 0x0A) {
        TA0R = TA0_OFFSET;
        // Reset state
        TA0CCTL0 = TA0CCTL1 = OUTMOD_0 + OUT;
        TA0CCTL0 = TA0CCTL1 = OUTMOD_5;
    }
}

#pragma vector=TIMER1_A1_VECTOR
__interrupt void TA1_ISR(void)
{
    if (TA1IV & 0x0A) {
        TA1R = TA1_OFFSET;
        if (fan_boost) {
            --fan_boost;
            if (!fan_boost) {
                WIND_PWM = pwm_fill(FAN_PWM_PERIOD, wind_pwm_lvl);
                COOL_PWM = pwm_fill(FAN_PWM_PERIOD, cool_pwm_lvl);
            }
        }
        if (temp_measure_period) {
            --temp_measure_period;
            if (!temp_measure_period) {
                start_ADC_conv();
            }
        }
        // Reset state
        TA1CCTL0 = TA1CCTL1 = TA1CCTL2 = OUTMOD_0 + OUT;
        TA1CCTL0 = TA1CCTL1 = TA1CCTL2 = OUTMOD_5;
    }
}

#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
    int i;
    unsigned adc_temp = 0;

    ADC10CTL0 &= ~ENC;
    for (i = 0; i < MEASURE_CNT; i++) {
        adc_temp += temp_measure_series[i];
    }
    adc_temp /= MEASURE_CNT;
    celsius_temp = adc_to_celsius(adc_temp);

    // Actor control
    if (celsius_temp < keep_temp) {
        P1OUT &= ~HEATER;
    } else {
        P1OUT |= HEATER;
    }

    temp_measure_period = TEMP_MEASURE_PERIOD;
}

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
    UCA0TXBUF = out_str[str_iter++];
    if (out_str[str_iter-1] == '\0') {
        // Reset out_str iterator
        str_iter = 0;
        out_str = 0;
        // Disable USCI_A0 TX interrupt
        UC0IE &= ~UCA0TXIE;
        UCA0TXBUF = '\n';
        // Enable USCI_A0 RX interrupt
        UC0IE |= UCA0RXIE;
    }
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    cmd_buf[cmd_iter] = UCA0RXBUF;

    if (cmd_iter != 0) { // Number input was triggered
        if (cmd_buf[cmd_iter] < '0' || cmd_buf[cmd_iter] > '9') {
            // Skip non-number symbols
            return;
        } else {
            // Wait till 3 numbers are accepted
            if (++cmd_iter < sizeof(cmd_buf) - 1) return;
        }
    }

    switch (cmd_buf[0]) {
        case 'W':
        case 'R':
        case 'B':
        case 'C':
        case 'S':
        case 'T':
        case 'V':
            if (cmd_iter == 0) { // Trigger number input
                cmd_iter = 1;
            } else { // cmd_buf was filled
                cmd_iter = 0;
                set_cmd_handler(cmd_buf[0], atoi(&cmd_buf[1]));
            }
            break;
        case 'w':// Get white led level
            out_str = itoa(white_pwm_lvl, number_str, 10);
            break;
        case 'r':// Get red led level
            out_str = itoa(red_pwm_lvl, number_str, 10);
            break;
        case 'b':// Get blue led level
            out_str = itoa(blue_pwm_lvl, number_str, 10);
            break;
        case 'c':// Get cooling fan level
            out_str = itoa(cool_pwm_lvl, number_str, 10);
            break;
        case 's':// Get wind fan level
            out_str = itoa(wind_pwm_lvl, number_str, 10);
            break;
        case 't':// Get temperature
            out_str = itoa(celsius_temp, number_str, 10);
            break;
        default:
            out_str = unknown_str;
    }

    if (out_str) {
        // Disable USCI_A0 RX interrupt while processing TX
        UC0IE &= ~UCA0RXIE;
        // Enable USCI_A0 TX interrupt and start TX
        UC0IE |= UCA0TXIE;
        UCA0TXBUF = out_str[str_iter++];
    }
}

int main(void)
{
    P1OUT &= 0;
    P2OUT &= 0;

    setup_SMCLK();
    setup_UART();
    setup_PWM();
    setup_ADC();

    // Low power mode
    __bis_SR_register(CPUOFF + GIE);
    // Wait for interrupt
    while (1) { /* Loop */ }

    return 0;
}
